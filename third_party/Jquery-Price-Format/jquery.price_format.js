/*

* Price Format jQuery Plugin
* Created By Eduardo Cuducos cuducos [at] gmail [dot] com
* Currently maintained by Flavio Silveira flavio [at] gmail [dot] com
* Version: 1.7
* Release: 2012-02-22

* original char limit by Flavio Silveira <http://flaviosilveira.com>
* original keydown event attachment by Kaihua Qi
* keydown fixes by Thasmo <http://thasmo.com>
* Clear Prefix on Blur suggest by Ricardo Mendes from PhonoWay
* original allow negative by Cagdas Ucar <http://carsinia.com>
* keypad fixes by Carlos Vinicius <http://www.kvinicius.com.br> and Rayron Victor
* original Suffix by Marlon Pires Junior
*/

(function($) {

    /****************
    * Main Function *
    *****************/
    $.fn.priceFormat = function(options)
    {

        var defaults =
        {
            prefix: 'US$ ',
            suffix: '',
            centsSeparator: '.',
            thousandsSeparator: ',',
            limit: false,
            centsLimit: 2,
            clearPrefix: false,
            clearSufix: false,
            allowNegative: false,
            minusBehavior: 'default'
        };

        var options = $.extend(defaults, options);

        return this.each(function()
        {

            // pre defined options
            var obj = $(this);
            var is_number = /[0-9]/;

            // load the pluggings settings
            var prefix = options.prefix;
            var suffix = options.suffix;
            var centsSeparator = options.centsSeparator;
            var thousandsSeparator = options.thousandsSeparator;
            var limit = options.limit;
            var centsLimit = options.centsLimit;
            var clearPrefix = options.clearPrefix;
            var clearSuffix = options.clearSuffix;
            var allowNegative = options.allowNegative;
            var minusBehavior = options.minusBehavior;

            // skip everything that isn't a number
            // and also skip the left zeroes
            function to_numbers (str)
            {
                var formatted = '';
                for (var i=0;i<(str.length);i++)
                {
                    char_ = str.charAt(i);
                    if (formatted.length==0 && char_==0) char_ = false;

                    if (char_ && char_.match(is_number))
                    {
                        if (limit)
                        {
                            if (formatted.length < limit) formatted = formatted+char_;
                        }
                        else
                        {
                            formatted = formatted+char_;
                        }
                    }
                }

                return formatted;
            }

            // format to fill with zeros to complete cents chars
            function fill_with_zeroes (str)
            {
                return str;
                while (str.length<(centsLimit+1)) str = '0'+str;
                return str;
            }

            function formatThousands(number)
            {
                var numberStr = number.toString();
                var parts  = [];
                while (numberStr.length > 3)
                {
                    parts.unshift(numberStr.slice(-3));
                    numberStr = numberStr.slice(0, -3);
                }
                parts.unshift(numberStr);
                return parts.join(thousandsSeparator);
            }
            function formatCents(number, maxLength)
            {
                if (typeof number == 'number') 
                {
                    maxLength = Math.min(centsLimit, maxLength);
                    return centsSeparator + str_pad(number.toString().slice(0, maxLength), '0', 0, false);
                }
                else
                {
                    return centsSeparator;
                }
            }
            function str_pad(str, padChar, padLength, right)
            {
                if (typeof right == 'undefined')
                {
                    right = true;
                }
                while (str.length < padLength)
                {
                    if (right) 
                    {
                        str = str + padChar;
                    } else {
                        str = padChar + str;
                    }

                }
                return str;

            }
            // format as price
            function price_format (str)
            {
		// If string is empty just return it.
                if (str.length == 0) { return str; }
                if (str.slice(0, 1) == '-')
                {
                    var pref = '-';
                    if (str.length == 1) { return str; }
                }
                else
                {
                    var pref = '';
                }
                if (centsLimit > 0)
                {
                    var sep = str.indexOf(centsSeparator);
                    if (sep != -1)
                    { 
                        var centsStr = to_numbers(str.slice(sep));
                        if (centsStr.length == 0)
                        {
                            var centsVal = false;
                        }
                        else 
                        {
                            var centsVal = parseInt(str_pad(to_numbers(str.slice(sep)), '0', centsLimit), 10);
                        }
                        var val = parseInt('0' + to_numbers(str.slice(0,sep)), 10);
                    }
                    else
                    {
                        var val = parseInt('0' + to_numbers(str), 10);
                    }
                }
                else
                {
                    var val = parseInt('0' + to_numbers(str), 10);
                } 
                newValue = formatThousands(val);
                if (typeof centsVal != 'undefined')
                {
                    newValue = newValue + formatCents(centsVal, centsStr.length);
                }

                return pref + newValue;

            }

            // filter what user type (only numbers and functional keys)
            function key_check (e)
            {
                var code = e.which;
                var functional = false;
                var str = obj.val();

                // allow key numbers, 0 to 9
                if((code >= 48 && code <= 57) || (code >= 96 && code <= 105)) functional = true;
                
                // check Backspace, Tab, Enter, Delete, and left/right arrows
                var funcKeys = [8, 9, 13, 46, 37, 39, 35, 36];
 
                // Check minus key
                if (allowNegative && $(this).caret()[0] == 0)
                {
                    funcKeys.push(173);
                    funcKeys.push(109);
                } 
                if (funcKeys.indexOf(code) > -1)
                {
                    functional = true;
                }
                if (!functional)
                {
                    e.preventDefault();
                    e.stopPropagation();
                    return false;
                }

            }


            // insert formatted price as a value of an input field
            function price_it (e)
            {
                var str = obj.val();
                var price = price_format(str);
                if (str != price)
                {
                    if (e && $().caret)
                    {
                        var pos = $(e.target).caret()[0];
                        pos = pos + price.length - str.length;
                        obj.val(price);
                        $(e.target).caret(pos);
                    } 
                    else
                    {
                        obj.val(price);
                    }
                }
            }      

            // Add prefix on focus
            function add_prefix()
            {
                var val = obj.val();
                obj.val(prefix + val);
            }
            
            function add_suffix()
            {
                var val = obj.val();
                obj.val(val + suffix);
            }

            // Clear prefix on blur if is set to true
            function clear_prefix()
            {
                if($.trim(prefix) != '' && clearPrefix)
                {
                    var array = obj.val().split(prefix);
                    obj.val(array[1]);
                }
            }
            
            // Clear suffix on blur if is set to true
            function clear_suffix()
            {
                if($.trim(suffix) != '' && clearSuffix)
                {
                    var array = obj.val().split(suffix);
                    obj.val(array[0]);
                }
            }

            // bind the actions
//            $(this).bind('keydown.price_format', key_check);
//              $(this).bind('change.price_format', change);
//            $(this).bind('keypress.price_format', price_it);
            $(this).bind('keyup.price_format', price_it);
//            $(this).bind('focusout.price_format', price_it);

            // Clear Prefix and Add Prefix
            if(clearPrefix)
            {
                $(this).bind('focusout.price_format', function()
                {
                    clear_prefix();
                });

                $(this).bind('focusin.price_format', function()
                {
                    add_prefix();
                });
            }
            
            // Clear Suffix and Add Suffix
            if(clearSuffix)
            {
                $(this).bind('focusout.price_format', function()
                {
                    clear_suffix();
                });

                $(this).bind('focusin.price_format', function()
                {
                    add_suffix();
                });
            }

            // If value has content
            if ($(this).val().length>0)
            {
                price_it();
                clear_prefix();
                clear_suffix();
            }

        });

    };
    
    /**********************
    * Remove price format *
    ***********************/
    $.fn.unpriceFormat = function(){
      return $(this).unbind(".price_format");
    };

    /******************
    * Unmask Function *
    *******************/
    $.fn.unmask = function(){

        var field = $(this).val();
        var result = "";

        for(var f in field)
        {
            if(!isNaN(field[f]) || field[f] == "-") result += field[f];
        }

        return result;
    };

})(jQuery);
